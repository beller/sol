This is the historic version of SOL.

File naming-scheme: instruments-playing_technique-pitch-dynamic-free_infos.wav 

Examples:

*  Cb-harm_nat_gliss_med_up-N-p-1c.wav
*  Hp-buzz_pedal-B1-f.wav
*  Bn-blow_no_reed-A#1-mf=pp.wav

Note the presence of the character "N" that can be applied both for samples without pitch or dynamic info (if needed). 
If no dynamic and no pitch are present use : mysound-exemple-N-N.wav. 
Also note the mf+pp that allows to describe a plying technique played mf resulting pp (this is sometime needed for Loudness factor correction)

warning: Sounds are normalized
